dmg_package "IntelliJ IDEA 12" do
  dmg_name "idea"
  source "http://download.jetbrains.com/idea/ideaIU-12.0.1.dmg"
  action :install
  owner WS_USER
end
